angular.module('atom', ['ui.router'])

    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {
                url: "/",
                templateUrl: "view/home.html",
                controller: 'homeController'
            })

            .state('product', {
                url: "/product/:title/:image/:price/:oldPrice",
                templateUrl: "view/product.html",
                controller: 'productController'
            })


            .state('cart', {
                url: "/cart",
                templateUrl: "view/cart.html",
                controller: 'cartController'
            })
    })