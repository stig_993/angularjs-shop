angular.module('atom')
    .controller('headerController', headerController)
    .controller('homeController', homeController)
    .controller('productController', productController)
    .controller('cartController', cartController)
    .run(function ($rootScope) {

        if (localStorage.getItem('cart') == undefined || localStorage.getItem('cart').length == 2) {
            $rootScope.cart = [];
        } else {
            $rootScope.cart = localStorage.getItem('cart');
        }

        if ($rootScope.cart.length != 0) {
            $rootScope.cart = JSON.parse(localStorage.getItem('cart'));

            function sum(num1, num2) {
                return num1 + num2;
            }

            var cartSum = []
            for (var i = 0; i < $rootScope.cart.length; i++) {
                cartSum.push(parseInt($rootScope.cart[i].price));
            }
            $rootScope.cartSum = cartSum.reduce(sum);
        }

    })

function headerController() {
    // Mobile menu animation
    var activeBurger = true;
    $('.burgerParent').on('click', function () {
        if (activeBurger) {
            $('body').css('overflow-y', 'hidden');
            $('.burgerParent').css({
                "position": "fixed",
                "top": "10px",
                "right": "20px"
            });
            $('.mobileMenu').css('left', '0');
            $(".burger1, .burger2, .burger3").css("background", 'white');
            $('.burger2').css('opacity', '0');
            $('.burger1').css({
                "transform": "rotate(45deg)",
                "top": "+=10px"
            });
            $('.burger3').css({
                "transform": "rotate(-45deg)",
                "top": "-=9px"
            });
            activeBurger = false;
        } else {
            $('body').css('overflow-y', 'scroll');
            $('.burgerParent').css({
                "position": "relative",
                "top": "0",
                "right": "0"
            })
            $('.mobileMenu').css('left', '-100%');
            $(".burger1, .burger2, .burger3").css("background", '#932d51');
            $('.burger2').css('opacity', '1');
            $('.burger1').css({
                "transform": "rotate(0deg)",
                "top": "-=10px"
            });
            $('.burger3').css({
                "transform": "rotate(0deg)",
                "top": "+=9px"
            });
            activeBurger = true;
        }
    })
    ///
    var activeSub = true;
    $('.mobileMenu .sub').on('click', function () {
        if (activeSub) {
            $('.mobileMenu').css('overflow-y', 'scroll');
            activeSub = false;
        } else {
            $('.mobileMenu').css('overflow-y', 'hidden');
            activeSub = true;
        }
        $(this).parent().toggleClass('activeSub')
        $(this).find('.caret').toggleClass('caretDirection');
        $(this).children('.submenu').toggleClass('showSubmenu');
    })


}

function homeController($scope, $rootScope, $state) {

    // Slider
    $scope.slider = [
        {image: "slider.jpg", id: 1, active: false, allModels: true, allBalsamic: false},
        {image: "slider.jpg", id: 2, active: true, allModels: false, allBalsamic: true},
        {image: "slider.jpg", id: 3, active: false, allModels: true, allBalsamic: false}
    ];

    $scope.changeSlide = function (index) {
        for (var i = 0; i < $scope.slider.length; i++) {
            $scope.slider[i].active = false;
        }
        $scope.slider[index].active = true;
    }
    //////////////////

    $scope.tabs = [
        {tabName: "Hot products", active: false},
        {tabName: "Latest products", active: true},
        {tabName: "Best seller", active: false}
    ]

    $scope.changeTab = function (index) {
        for (var i = 0; i < $scope.tabs.length; i++) {
            $scope.tabs[i].active = false;
        }
        $scope.tabs[index].active = true;
    }

    // Prevent page redirect if product is out of stock
    $scope.changeState = function (p) {
        if (p.available) {
            $state.go('product', {title: p.title, image: p.image, price: p.price, oldPrice: p.oldPrice})
        }
    }

    // Products
    $scope.products = [
        {
            image: "1.jpg",
            title: "Face care pack",
            price: "120",
            oldPrice: "",
            sale: false,
            new: false,
            available: true
        },
        {
            image: "2.jpg",
            title: "Skin care pack",
            price: "42",
            oldPrice: "120",
            sale: false,
            new: false,
            available: true
        },
        {
            image: "3.jpg",
            title: "Bath & Body pack",
            price: "169",
            oldPrice: "",
            sale: false,
            new: false,
            available: true
        },
        {
            image: "4.jpg",
            title: "Face care pack",
            price: "68",
            oldPrice: "",
            sale: true,
            new: false,
            available: true
        },
        {
            image: "5.jpg",
            title: "Face care pack",
            price: "38.99",
            oldPrice: "",
            sale: false,
            new: true,
            available: true
        },
        {
            image: "6.jpg",
            title: "Face care pack",
            price: "25",
            oldPrice: "",
            sale: false,
            new: true,
            available: true
        },
        {
            image: "7.jpg",
            title: "Bath & Body pack",
            price: "169",
            oldPrice: "",
            sale: false,
            new: false,
            available: false
        },
        {
            image: "8.jpg",
            title: "Face care pack",
            price: "169",
            oldPrice: "",
            sale: true,
            new: false,
            available: true
        }
    ]

    setTimeout(function () {
        $('.breakFlag').parent().after("<div class='breakLine'></div>")
    }, 50)

    // Cart

    function sum(num1, num2) {
        return num1 + num2;
    }

    $scope.addToCart = function (product) {
        product.quant = 1;
        $rootScope.cart.push(product);
        localStorage.setItem('cart', JSON.stringify($rootScope.cart));
        var cartSum = []
        for (var i = 0; i < $rootScope.cart.length; i++) {
            cartSum.push(parseInt($rootScope.cart[i].price));
        }
        $rootScope.cartSum = cartSum.reduce(sum);
    }
}

function productController($scope, $stateParams, $rootScope) {

    // Products
    $scope.products = [
        {
            image: "1.jpg",
            title: "Face care pack",
            price: "120",
            oldPrice: "",
            sale: false,
            new: false,
            available: true
        },
        {
            image: "2.jpg",
            title: "Skin care pack",
            price: "42",
            oldPrice: "120",
            sale: false,
            new: false,
            available: true
        },
        {
            image: "3.jpg",
            title: "Bath & Body pack",
            price: "169",
            oldPrice: "",
            sale: false,
            new: false,
            available: true
        },
        {
            image: "4.jpg",
            title: "Face care pack",
            price: "68",
            oldPrice: "",
            sale: true,
            new: false,
            available: true
        },
        {
            image: "5.jpg",
            title: "Face care pack",
            price: "38.99",
            oldPrice: "",
            sale: false,
            new: true,
            available: true
        },
        {
            image: "6.jpg",
            title: "Face care pack",
            price: "25",
            oldPrice: "",
            sale: false,
            new: true,
            available: true
        },
        {
            image: "7.jpg",
            title: "Bath & Body pack",
            price: "169",
            oldPrice: "",
            sale: false,
            new: false,
            available: false
        },
        {
            image: "8.jpg",
            title: "Face care pack",
            price: "169",
            oldPrice: "",
            sale: true,
            new: false,
            available: true
        }
    ];

    $scope.product = $stateParams;

    // Preview product image
    $scope.viewImage = function (image) {
        $('.previewImage').css({
            "opacity": "1",
            "visibility": "visible"
        })
        $('body').css('overflow-y', 'hidden');
        $scope.image = image;
    }

    $('.closeDiv').on('click', function () {
        $('body').css('overflow-y', 'scroll');
        $('.previewImage').css({
            "opacity": "0",
            "visibility": "hidden"
        })
    })
    //////

    // Expand Description tab
    $(".plus").on('click', function () {
        $(this).parent().parent().children('.content').toggleClass('myCollapse');
    })

    // Price math
    $scope.quant = 1;
    var newPrice = $scope.product.price;
    var originalPrice = $scope.product.price;
    $scope.increment = function () {
        $scope.quant++;
        $scope.product.price = newPrice * $scope.quant;
    }
    $scope.decrement = function () {
        // Block if quant is 1
        if ($scope.quant == 1) {
            $scope.quant = 1;
        } else {
            $scope.quant--;
            $scope.product.price = newPrice * $scope.quant;
        }
    }

    $scope.$watch('quant', function (res) {
        if (res >= 0) {
            $scope.product.price = newPrice * res;
        }
    })

    ///

    function sum(num1, num2) {
        return num1 + num2;
    }

    $scope.addToCart = function (product) {
        product.originalPrice = originalPrice;
        $rootScope.cart.push(product);
        product.quant = $scope.quant;
        localStorage.setItem('cart', JSON.stringify($rootScope.cart));
        var cartSum = []
        for (var i = 0; i < $rootScope.cart.length; i++) {
            cartSum.push(parseInt($rootScope.cart[i].price));
        }
        $rootScope.cartSum = cartSum.reduce(sum);
    }

}

function cartController($scope, $rootScope) {
    $scope.cart = $rootScope.cart;

    function sum(num1, num2) {
        return num1 + num2;
    }

    $scope.$watch('c.quant', function (res) {
        $scope.cart.price = res * $scope.cart.price;
    })


    var sumCart = [];

    for (var i = 0; i < $scope.cart.length; i++) {
        $scope.cart[i].mathPrice = $scope.cart[i].price;
    }

    $scope.quantChange = function (index, quant) {
        if ($scope.cart[index].originalPrice == undefined) {
            $scope.cart[index].originalPrice = $scope.cart[index].price;
        }

        if (quant >= 1 || quant != "") {
            var newPrice = $scope.cart[index].mathPrice * quant;
            $scope.cart[index].price = newPrice;
            $rootScope.cart[index].price = newPrice;
            sumCart = [];
            calculateTotal();
        }
    }

    $scope.removeFromCart = function (index) {
        $rootScope.cart.splice(index, 1);
        sumCart.splice(index, 1);
        localStorage.setItem('cart', JSON.stringify($rootScope.cart));
        if (sumCart.length != 0) {
            $scope.orderSubTotal = sumCart.reduce(sum);
            $scope.tax = ($scope.orderSubTotal * 7) / 100;
            $scope.orderTotal = $scope.orderSubTotal + $scope.tax;
            $rootScope.cartSum = $scope.orderSubTotal;
        } else {
            $scope.orderSubTotal = 0;
            $scope.tax = 0;
            $scope.orderTotal = 0;
            $rootScope.cartSum = 0;
        }
    };
    
    function calculateTotal() {
        if ($rootScope.cart.length != 0) {
            for (var i = 0; i < $rootScope.cart.length; i++) {
                sumCart.push(parseInt($rootScope.cart[i].price));
            }
            $scope.orderSubTotal = sumCart.reduce(sum);
            $scope.tax = ($scope.orderSubTotal * 7) / 100;
            $scope.orderTotal = $scope.orderSubTotal + $scope.tax;
        }
    }
    calculateTotal();

}